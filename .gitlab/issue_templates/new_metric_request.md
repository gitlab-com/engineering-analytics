## New Metric Request

### Description

Please provide some context about this new metric:

- [ ] What is the name of the (K)PI?
- [ ] Does the (K)PI have a definition?
- [ ] Does the (K)PI have a target?
- [ ] How far should the data look back? What time interval would you like the data to be visualised?
- [ ] Is this data SAFE?


### Outcome

- [ ] Should this chart live in an existing dashboard [link to dashboard]? Or a new one?
- [ ] Should this chart be embedded in an existing handbook page? [link to page]

### Additional comments

