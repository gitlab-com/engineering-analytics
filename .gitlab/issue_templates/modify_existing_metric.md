## Modify Existing Metric

### Description

Please provide some context why this metric needs to be modified.

- [ ] What is the name of the (K)PI? [link to chart]
- [ ] What is the current definition of the metric?
- [ ] What will be the new definition of the metric?
- [ ] Does the modified metric have a new target?
- [ ] How far should the data look back? What time interval would you like the data to be visualised?
- [ ] Is this data SAFE?


### Outcome

- [ ] Should the modified chart replace the existing one in its dashboard [link to dashboard]? Or should it be in a new one?
- [ ] Should this chart be embedded in an existing handbook page? [link to page]

### Additional comments

